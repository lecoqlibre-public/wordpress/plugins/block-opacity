<?php

/**
 * Plugin Name: Lecoqlibre Block Opacity
 * Plugin URI: https://framagit.org/lecoqlibre-public/wordpress/plugins/block-opacity
 * Description: Add a bg-opacity-XX CSS class to add background opacity.
 * Author: Maxime Lecoq (lecoqlibre)
 * Author URI: https://framagit.org/lecoqlibre
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: lecoqlibre-block-opacity
 *
 * @package Lecoqlibre
 *
 */

/** Add an alpha to the background-color of any block that support bg based onto the CSS class bg-opacity-%% */
add_filter( 'render_block', function( string $block_content, array $block ) {
    if( ! isset( $block[ 'attrs' ][ 'className' ] ) )
        return $block_content;
  
    if( ! isset( $block[ 'attrs' ][ 'style' ][ 'color' ][ 'background' ] ) && ! isset( $block[ 'attrs' ][ 'backgroundColor' ] ) )
        return $block_content;
  
    $classes = explode( ' ', $block[ 'attrs' ][ 'className' ] ); // classes should be separated with space
    $mask = 'bg-opacity-'; // the base CSS class name to search for
    $opacity = array();
    
    // Search for the opacity through the CSS classes, the first one will be used
    foreach( $classes as $class ) {
        $match = array();
        if( ! 1 == preg_match( "#{$mask}([0-9]+)#", $class, $match ) )
            continue;
        $opacity = intval( $match[ 1 ] );
        break;
    }
    
    if( is_array( $opacity ) && empty( $opacity ) )
        return $block_content;
  
    // Compute the alpha value
    $alpha = dechex( 255 * $opacity / 100 );
  
    // Add a background color with an alpha when using predefined color name
    if( isset( $block[ 'attrs' ][ 'backgroundColor' ] ) ) {
        $colorName = $block[ 'attrs' ][ 'backgroundColor' ];
        $colors = array(
            'pale-pink' => '#f78da7', 
            'vivid-red' => '#cf2e2e', 
            'luminous-vivid-orange' => '#ff6900', 
            'luminous-vivid-amber' => '#fcb900', 
            'light-green-cyan' => '#7bdcb5', 
            'vivid-green-cyan' => '#00d084', 
            'pale-cyan-blue' => '#8ed1fc', 
            'vivid-cyan-blue' => '#0693e3', 
            'vivid-purple' => '#9b51e0', 
            'white' => '#ffffff', 
            'very-light-gray' => '#eeeeee', 
            'cyan-bluish-gray' => '#abb8c3', 
            'very-dark-gray' => '#313131', 
            'black' => '#000000'
            );

        // Add predefined colors from theme.json
        $json = get_stylesheet_directory() . '/theme.json';
        if( file_exists( $json ) ) {
            $theme = json_decode( file_get_contents( $json ) );
            if( isset( $theme->settings->color->palette ) ) {
                foreach( $theme->settings->color->palette as $color )
                    $colors[ $color->slug ] = $color->color;
            }
        }

        $color = $colors[ $colorName ] . $alpha;
        $block_content = preg_replace( '#style="#', "style=\"background-color:{$color} !important;", $block_content, 1 );
    }
  
    // Replace the background color with the alpha when using custom color
    else {
        $color = $block[ 'attrs' ][ 'style' ][ 'color' ][ 'background' ] . $alpha;
        $block_content = preg_replace( '#background-color:(.+);#', "background-color:{$color};", $block_content, 1 );
    }
    
    return $block_content;
  }, 10, 2 );
